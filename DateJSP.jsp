
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.text.*,java.util.*" %>
<html>
 <link rel="stylesheet" href="DateJSP.css" type="text/css"
 <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&display=swap" rel="stylesheet">
<head><title>First JSP</title></head>
<body>
  <div>
  <%
    GregorianCalendar time = new GregorianCalendar();
    int hour = time.get(Calendar.HOUR_OF_DAY);
	int min = time.get(Calendar.MINUTE);
    if (hour < 12) {
  %>
      <h2>Hurry up! You should have your breakfast!</h2>	

  <%
    } else if (hour>=12 && hour<=16){
  %>

      <h2>Yeyy! It's luch time! </h2>
  <%
    }
	else {
  %>
      <h2>Good evening! Dinner is ready! </h2>
  <%
    }
  %>
  
   <p> Current time: (<%= hour %> : <%= min %>)</p>
    </div>
</body>
</html>